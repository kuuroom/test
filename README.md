# PHPインストール（PHP7.0）  

#### PHPインストール  
```
# yum install epel-release
# rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
# yum install --enablerepo=remi,remi-php70 php php-devel php-mbstring php-pdo php-gd
```

### PHPアクセラレータのインストール  
```
# yum install --enablerepo=epel,remi-php70 php-opcache php-pecl-apcu
```

### Apacheの再起動  
```
# systemctl restart httpd.service
```

# Gitインストール  
### IUSリポジトリからGitの最新版をインストール  
```
# yum -y install https://centos7.iuscommunity.org/ius-release.rpm
# yum -y install --enablerepo=ius git2u
```

### IUSリポジトリの無効化  
```
# yum -y install yum-utils
# yum-config-manager --disable ius
```

### インストール確認  
```
# git --version
git version 2.16.5
```

# FuelPHPインストール  
### mercuryユーザーを作成し、アクセス権変更  
```
# useradd mercury
# passwd mercury
# chmod 755 /home/mercury/
```

### oil（FuelPHP用クイックインストーラ）をインストール  
```
# curl https://get.fuelphp.com/oil | sh
```

### リモートリポジトリからクローン  
```
$ git clone https://kumiko-ohara@bitbucket.org/hackest/mercury-php.git fuelphp
$ git checkout develop
```

### FuelPHPプロジェクトを作成  
```
$ cd /home/mercury/
$ oil create fuelphp 
$ chmod -R 777 /home/mercury/fuelphp/fuel/app/logs
```

### シンボリックリンクを張る  
```
$ ln -s /home/mercury/fuelphp/public /var/www/html/fuel
```

以下にアクセス！  
https://mercury.yokozunagames.net/fuel/

